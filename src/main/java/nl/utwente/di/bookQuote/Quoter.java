package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    HashMap<String,Double> prices = new HashMap<>();

    public Quoter(){

        prices.put("1",10.0);
        prices.put("2",45.0);
        prices.put("3",20.0);
        prices.put("4",35.0);
        prices.put("5",50.0);

    }

    double getBookPrice(String isbn){
        if(Integer.parseInt(isbn) > 5)
            return 0;
        else
            return prices.get(isbn);
    }

}